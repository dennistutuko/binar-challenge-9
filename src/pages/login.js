import React, { Component } from "react";
import { Container, Form, Label, Input } from "reactstrap";
import Navbar from "../components/navbar";
import firebase from "../services/firebase";

class Login extends Component {
  state = {
    email: "",
    password: "",
  };

  set = (name) => (event) => {
    event.preventDefault();
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) alert("Mohon masukkan email dan password!");
    firebase.auth().signInWithEmailAndPassword(email, password);
    alert("Anda berhasil masuk!");
  };

  render() {
    return (
      <>
        <Navbar />
        <Container>
          <Form>
            <Label>Email: </Label>
            <input
              type="text"
              placeholder="Email"
              onChange={this.set("email")}
            />
            <br />
            <br />
            <Label>Password: </Label>
            <input
              type="password"
              placeholder="Password"
              onChange={this.set("password")}
            />
            <br />
            <br />
            <Button color="primary" title="Login" click={} />
          </Form>
        </Container>
      </>
    );
  }
}

export default Login;
