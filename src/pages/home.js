import React, { Component } from "react";
import Nav from "../components/navbar";
import Button from "../components/button";

import "../assets/css/homepage/header.css";

class Home extends Component {
  state = {};
  render() {
    return (
      <>
        <Nav />
        <div class="header">
          <div class="content">
            <h1 class="title">PLAY TRADITIONAL GAME</h1>
            <p class="text">Experience new traditional game play</p>
            <Button url="/gamelist" title="PLAY NOW" color="danger" />
          </div>
        </div>
      </>
    );
  }
}

export default Home;
