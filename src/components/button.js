import React from "react";
import { Button } from "reactstrap";

import { Link } from "react-router-dom";

export default (props) => {
  return (
    <Button color={props.color} onClick={props.click}>
      <Link to={props.url}>{props.title}</Link>
    </Button>
  );
};
