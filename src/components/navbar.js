import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

import "../assets/css/homepage/navbar.css";

import { Link } from "react-router-dom";

const NavigationBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="dark" light expand="md">
        <NavbarBrand>
          <Link to="/">LOGO</Link>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Nav className="mr-auto" navbar>
          <NavItem>
            <NavLink>
              <Link to="/">HOME</Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link to="/">WORK</Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link to="/">CONTACT</Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link to="/">ABOUT ME</Link>
            </NavLink>
          </NavItem>
        </Nav>
      </Navbar>
    </div>
  );
};

export default NavigationBar;
