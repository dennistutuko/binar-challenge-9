import firebase from "firebase";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA-aQFTj4sN5psEu4xFhS4ZXcgH2ktQcdY",
  authDomain: "binar-challenge-9-fad6c.firebaseapp.com",
  databaseURL:
    "https://binar-challenge-9-fad6c-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "binar-challenge-9-fad6c",
  storageBucket: "binar-challenge-9-fad6c.appspot.com",
  messagingSenderId: "587941738571",
  appId: "1:587941738571:web:127e30f14591dd352475ce",
  measurementId: "G-DCLGWY0P0S",
};
// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);

export default app;
